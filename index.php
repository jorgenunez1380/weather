<?php 
    include_once('search.php');
    include_once('conn.php');

    if (isset($_GET["submit"])) {
        
        $city = isset($_GET['city']) ? $_GET['city'] : 'Bogota' ;
        $city = $_GET['city'] != "" ? $_GET['city'] : 'Bogota';
        $url = "api.openweathermap.org/data/2.5/weather?q=".$city."&appid=dd56f0ca78bab55d8f4ab05a7e5c843e";
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $resultSearch = curl_exec($ch);
        curl_close($ch);
        $resultSearch =json_decode($resultSearch, true);

        try{
            $consultaSQL = "INSERT INTO records (record)";
            $consultaSQL .= "values (:record)";
            $sentencia = $conexion->prepare($consultaSQL);
            $sentencia->execute(["record" => $city]);
        }catch(PDOException $error){
            print_r($error->getMessage());
        }
        
        try{
            $consultaSQL = "SELECT * FROM records";
            $sentencia = $conexion->prepare($consultaSQL);
            $sentencia->execute();
            $records = $sentencia->fetchAll();
        }catch(PDOException $error){
            print_r($error->getMessage());
        }

    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>weather</title>

    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <div class="container">
        <div class="weather"> 
            <img src="<?php echo ("http://openweathermap.org/img/wn/". $result['weather'][0]['icon'] . "@2x.png" ) ?>" alt="">
            <label class="label oscuro"><?php echo $result['name'] ?></label>
            <label class="label claro"><?php echo ($result['main']['temp'] - 273.15)?>° C</label>
        </div>
        <div class="weather">
            <img src="<?php echo ("http://openweathermap.org/img/wn/". $result2['weather'][0]['icon'] . "@2x.png" ) ?>" alt="">
            <label class="label oscuro"><?php echo $result2['name'] ?></label>
            <label class="label claro"><?php echo ($result2['main']['temp'] - 273.15) ?>° C</label>
        </div>
        <div class="weather">
            <img src="<?php echo ("http://openweathermap.org/img/wn/". $result3['weather'][0]['icon'] . "@2x.png" ) ?>" alt="">
            <label class="label oscuro"><?php echo $result3['name'] ?></label>
            <label class="label claro"><?php echo ($result3['main']['temp'] - 273.15) ?>° C</label>
        </div>
    </div>
    <div class="container">
        <form id="formSearch" action="" method="get">
            <input type="text" name="city" id="city" value=""/>
            <br>
            <button type="submit" name="submit">Buscar</button>
        </form>
        <div class="weather">
            <img src="<?php echo ("http://openweathermap.org/img/wn/". $resultSearch['weather'][0]['icon'] . "@2x.png" ) ?>" alt="">
            <label class="label oscuro"><?php echo $resultSearch['name'] ?></label>
            <label class="label claro"><?php echo ($resultSearch['main']['temp'] - 273.15) ?>° C</label>
        </div>
        <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Busqueda</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($records && $sentencia->rowCount() > 0) {
            foreach ($records as $fila) {
              ?>
              <tr>
                <td><?php echo ($fila["id"]); ?></td>
                <td><?php echo ($fila["record"]); ?></td>
        <?php 
            }
          }     
        ?>
    </div>
</body>
</html>